#include "CancellationToken.h"


namespace util
{
    CancellationToken::CancellationToken()
            : m_mutex()
              , m_cancelled(false)
    {
    }

    void CancellationToken::cancel()
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_cancelled = true;
    }

    bool CancellationToken::is_cancelled() const
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        return m_cancelled;
    }
}