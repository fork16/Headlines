#include "HtmlParser.h"
#include "Helpers.h"
#include <stdio.h>
#include <string.h>
#include "AppSettings.h"

namespace util
{
    void HtmlParser::EscapeText(std::string& string)
    {
        auto it = string.find('&');
        while (it != std::string::npos)
        {
            string.replace(it, 1, "&amp;");
            it = string.find('&', it + 1);
        }

        it = string.find('<');
        while (it != std::string::npos)
        {
            string.replace(it, 1, "&lt;");
            it = string.find('<', it + 1);
        }

        it = string.find('>');
        while (it != std::string::npos)
        {
            string.replace(it, 1, "&gt;");
            it = string.find('<', it + 1);
        }
    }

    static std::unordered_map<std::string, HtmlParser::BlockType> html_to_block_type =
    {
            {"strong",     HtmlParser::BlockType::Text},
            {"em",         HtmlParser::BlockType::Text},
            {"s",          HtmlParser::BlockType::Text},
            {"sup",        HtmlParser::BlockType::Text},
            {"sub",        HtmlParser::BlockType::Text},
            {"u",          HtmlParser::BlockType::Text},
            {"del",        HtmlParser::BlockType::Text},
            {"h1",         HtmlParser::BlockType::Text},
            {"h2",         HtmlParser::BlockType::Text},
            {"h3",         HtmlParser::BlockType::Text},
            {"h4",         HtmlParser::BlockType::Text},
            {"h5",         HtmlParser::BlockType::Text},
            {"h6",         HtmlParser::BlockType::Text},
            {"blockquote", HtmlParser::BlockType::Quote},
            {"code",       HtmlParser::BlockType::Code},
            {"a",          HtmlParser::BlockType::Text},
            {"html",       HtmlParser::BlockType::Text},
            {"body",       HtmlParser::BlockType::Text},
            {"div",        HtmlParser::BlockType::Text},
            {"p",          HtmlParser::BlockType::Text},
            {"span",       HtmlParser::BlockType::Text},
            {"table",      HtmlParser::BlockType::Table},
            {"tr",         HtmlParser::BlockType::Table},
            {"th",         HtmlParser::BlockType::Table},
            {"td",         HtmlParser::BlockType::Table},
            {"thead",      HtmlParser::BlockType::Table},
            {"tbody",      HtmlParser::BlockType::Table},
            {"tfoot",      HtmlParser::BlockType::Table},
            {"ol",         HtmlParser::BlockType::Text},
            {"ul",         HtmlParser::BlockType::Text},
            {"li",         HtmlParser::BlockType::Text},
            {"hr",         HtmlParser::BlockType::Text},
            {"br",         HtmlParser::BlockType::Text},
            {"pre",        HtmlParser::BlockType::Text},
    };

    static std::unordered_map<std::string, HtmlParser::NodeType> html_to_node_type =
    {
            {"strong",     HtmlParser::NodeType::Text},
            {"em",         HtmlParser::NodeType::Text},
            {"s",          HtmlParser::NodeType::Text},
            {"sup",        HtmlParser::NodeType::Text},
            {"sub",        HtmlParser::NodeType::Text},
            {"u",          HtmlParser::NodeType::Text},
            {"del",           HtmlParser::NodeType::Text},
            {"h1",         HtmlParser::NodeType::Text},
            {"h2",         HtmlParser::NodeType::Text},
            {"h3",         HtmlParser::NodeType::Text},
            {"h4",         HtmlParser::NodeType::Text},
            {"h5",         HtmlParser::NodeType::Text},
            {"h6",         HtmlParser::NodeType::Text},
            {"blockquote", HtmlParser::NodeType::Quote},
            {"code",       HtmlParser::NodeType::Code},
            {"a",          HtmlParser::NodeType::Text},
            {"html",       HtmlParser::NodeType::Text},
            {"body",       HtmlParser::NodeType::Text},
            {"div",        HtmlParser::NodeType::Text},
            {"p",          HtmlParser::NodeType::Text},
            {"span",       HtmlParser::NodeType::Text},
            {"table",      HtmlParser::NodeType::Table},
            {"tr",         HtmlParser::NodeType::TableRow},
            {"th",         HtmlParser::NodeType::TableHeader},
            {"td",         HtmlParser::NodeType::TableData},
            {"thead",      HtmlParser::NodeType::TableHeadScope},
            {"tbody",      HtmlParser::NodeType::TableBodyScope},
            {"tfoot",      HtmlParser::NodeType::TableFootScope},
            {"ol",         HtmlParser::NodeType::Ul}, //TODO: support ordered lists.
            {"ul",         HtmlParser::NodeType::Ul},
            {"li",         HtmlParser::NodeType::Li},
            {"hr",         HtmlParser::NodeType::Hr},
            {"br",         HtmlParser::NodeType::Br},
            {"pre",        HtmlParser::NodeType::Pre},
    };

    static std::unordered_map<std::string, std::string> html_text_to_pango_start =
    {
        { "strong", "b" },
        { "em",     "i" },
        { "s",      "s" },
        { "sup",    "sup" },
        { "del",    "s" },
        { "sub",    "sub" },
        { "u",      "u" },
        { "h1",     "span size=\"x-large\"" },
        { "h2",     "span size=\"large\"" },
        { "h3",     "span size=\"medium\"" },
        { "a",      "a" }
    };

    static std::unordered_map<std::string, std::string> html_text_to_pango_end =
    {
        { "strong", "b" },
        { "em",     "i" },
        { "s",      "s" },
        { "sup",    "sup" },
        { "del",    "s" },
        { "sub",    "sub" },
        { "u",      "u" },
        { "h1",     "span" },
        { "h2",     "span" },
        { "h3",     "span" },
        { "a",      "a" }
    };

    HtmlParser::HtmlParser()
        : m_TextBlocks(std::vector<std::shared_ptr<Block>>())
        , m_ParentNodeType(NodeType::None)
        , m_TextLength()
        , m_MaxTextLength()
    {

    }

    void HtmlParser::ParseElement(xmlNode * node, bool hideSpoilers)
    {
        if (node == nullptr)
        {
            return;
        }

        if (m_MaxTextLength != -1 && m_TextLength >= m_MaxTextLength)
        {
            return;
        }

        for (xmlNode* cur_node = node; cur_node; cur_node = cur_node->next)
        {
            if (cur_node->type == XML_ELEMENT_NODE)
            {
                ParseNode(cur_node, hideSpoilers);
            }
            else if (cur_node->type == XML_TEXT_NODE)
            {
                if (m_ParentNodeType == NodeType::Table)
                {
                    ParseTableText(cur_node, false, hideSpoilers);
                }
                else
                {
                    ParseText(cur_node, false, hideSpoilers);
                }
            }
        }
    }

    void HtmlParser::ParseHtml(const std::string& html, Gtk::Box* parent, int maxTextLength, bool hideSpoilers)
    {
        if (html.empty())
        {
            return;
        }

        m_MaxTextLength = maxTextLength;

        std::string unescapedHtml = Helpers::UnescapeHtml(html);

        htmlDocPtr doc = htmlReadMemory(unescapedHtml.c_str(), (int)unescapedHtml.size(), "", NULL, HTML_PARSE_NOBLANKS | HTML_PARSE_NOERROR | HTML_PARSE_NOWARNING | HTML_PARSE_NONET);
        if (doc == nullptr)
        {
            return;
        }

        xmlNode* docRoot = xmlDocGetRootElement(doc);
        if (docRoot == nullptr)
        {
            xmlFreeDoc(doc);
            return;
        }

        ParseElement(docRoot, hideSpoilers);

        xmlFreeDoc(doc);
        xmlCleanupParser();

        if (m_MaxTextLength != -1 && m_TextLength > m_MaxTextLength && m_TextBlocks.back()->m_Type == BlockType::Text)
        {
            std::shared_ptr<TextBlock> textBlock = std::static_pointer_cast<TextBlock>(m_TextBlocks.back());
            textBlock->m_StringStream << "...";
        }

        for (const std::shared_ptr<Block>& block : m_TextBlocks)
        {
            switch (block->m_Type)
            {
                case BlockType::Text:
                {
                    const std::shared_ptr<TextBlock>& textBlock = std::static_pointer_cast<TextBlock>(block);
                    std::string content = textBlock->m_StringStream.str();
                    if (content.empty())
                    {
                        continue;
                    }

                    auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/text_label.ui");
                    Gtk::Label* label;
                    label = builder->get_widget<Gtk::Label>("Label");
                    label->set_use_markup(true);
                    label->set_markup(content);
                    label->signal_activate_link().connect([](const Glib::ustring& url)
                    {
                       util::Helpers::HandleLink(url);
                       return true;
                    }, false);

                    if (block->m_ContainsSpoilers)
                    {
                        Glib::RefPtr<Gtk::GestureClick> gestureClick = Gtk::GestureClick::create();
                        label->add_controller(gestureClick);
                        gestureClick->signal_released().connect([html, maxTextLength, parent, hideSpoilers](int n_press, double, double)
                        {
                            if (n_press == 0)
                            {
                                return;
                            }

                            for (Gtk::Widget* child = parent->get_last_child(); child != nullptr; child = child->get_prev_sibling())
                            {
                                parent->remove(*child);
                            }

                            util::HtmlParser htmlParser;
                            htmlParser.ParseHtml(html, parent, maxTextLength, !hideSpoilers);
                        });
                    }

                    parent->append(*label);
                    break;
                }
                case BlockType::Code:
                case BlockType::Quote:
                {
                    const std::shared_ptr<TextBlock>& textBlock = std::static_pointer_cast<TextBlock>(block);
                    std::string content = textBlock->m_StringStream.str();
                    if (content.empty())
                    {
                        continue;
                    }
                    auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/quote_label.ui");
                    Gtk::Label* label;
                    label = builder->get_widget<Gtk::Label>("Label");
                    label->set_use_markup(true);
                    label->set_markup(content);
                    label->signal_activate_link().connect([](const Glib::ustring& url)
                                                          {
                                                              util::Helpers::HandleLink(url);
                                                              return true;
                                                          }, false);

                    Gtk::Box* box;
                    box = builder->get_widget<Gtk::Box>("Box");

                    Gtk::DrawingArea* drawingArea;
                    drawingArea = builder->get_widget<Gtk::DrawingArea>("DrawingArea");
                    drawingArea->set_draw_func([](const Cairo::RefPtr<Cairo::Context>& cairoContext, int, int)
                    {
                        cairoContext->set_source_rgb(0.4, 0.4, 0.4);
                        cairo_paint((cairo_t *)cairoContext->cobj());
                    });

                    parent->append(*box);
                    break;
                }
                case BlockType::Table:
                {
                    const std::shared_ptr<TableBlock>& tableBlock = std::static_pointer_cast<TableBlock>(block);
                    auto table_builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/table.ui");
                    Gtk::Grid* grid;
                    grid = table_builder->get_widget<Gtk::Grid>("Grid");

                    int row = 0;
                    for (const std::vector<TableCell>& rowData : tableBlock->m_TableValues)
                    {
                        int column = 0;
                        for (const TableCell& value : rowData)
                        {
                            auto value_builder = Gtk::Builder::create_from_resource(value.m_IsHeader ? "/io/gitlab/caveman250/headlines/ui/table_header.ui" : "/io/gitlab/caveman250/headlines/ui/table_value.ui");

                            Gtk::Label* label;
                            label = value_builder->get_widget<Gtk::Label>("Label");
                            label->set_use_markup(true);
                            std::string markup = value.m_StringStream.str();
                            label->set_markup(markup);
                            label->signal_activate_link().connect([](const Glib::ustring& url)
                                                                  {
                                                                      util::Helpers::HandleLink(url);
                                                                      return true;
                                                                  }, false);

                            grid->attach(*label, column, row);

                            column++;
                        }

                        row++;
                    }

                    Gtk::ScrolledWindow* scrollWindow;
                    scrollWindow = table_builder->get_widget<Gtk::ScrolledWindow>("ScrolledWindow");

                    parent->append(*scrollWindow);
                    break;
                }
                case BlockType::None:
                {
                    //ignore
                    break;
                }
            }
        }

        m_TextBlocks.clear();
        m_ParentNodeType = NodeType::None;
    }

    void HtmlParser::ParseNode(xmlNode* node, bool hideSpoilers)
    {
        std::string nodeName((char *)node->name, strlen((char *)node->name));
        if (!VERIFY(html_to_block_type.find(nodeName) != html_to_block_type.end(), "Unhandled html node type \"%s\"", nodeName.c_str()))
        {
            return;
        }

        BlockType blockType = html_to_block_type[nodeName];
        bool beganSpecialBlock = false;

        if (m_ParentNodeType == NodeType::None && (m_TextBlocks.empty() || m_TextBlocks.back()->m_Type != blockType))
        {
            switch (blockType)
            {
                case BlockType::Table:
                {
                    std::shared_ptr<TableBlock> tableBlock = std::make_shared<TableBlock>();
                    tableBlock->m_Type = blockType;
                    m_TextBlocks.push_back(tableBlock);
                    beganSpecialBlock = true;
                    break;
                }
                case BlockType::Text:
                case BlockType::Code:
                case BlockType::Quote:
                {
                    std::shared_ptr<TextBlock> textBlock = std::make_shared<TextBlock>();
                    textBlock->m_Type = blockType;
                    m_TextBlocks.push_back(textBlock);
                    beganSpecialBlock = blockType != BlockType::Text;
                    break;
                }
                default:
                {
                    ASSERT(false, "HtmlParser::ParseNode - Unhandled block type: %i", (int)blockType);
                    break;
                }
            }
        }

        NodeType nodeType = html_to_node_type[nodeName];
        switch (nodeType)
        {
            case NodeType::Table:
            {
                m_ParentNodeType = NodeType::Table;
                ParseElement(node->children, hideSpoilers);
                m_ParentNodeType = NodeType::None;
                break;
            }
            case NodeType::TableHeadScope: //todo: do i need to render these?
            case NodeType::TableBodyScope: //todo: do i need to render these?
            case NodeType::TableFootScope: //todo: do i need to render these?
            {
                ParseElement(node->children, hideSpoilers);
                break;
            }
            case NodeType::TableRow:
            {
                std::shared_ptr<TableBlock> tableBlock = std::static_pointer_cast<TableBlock>(m_TextBlocks.back());
                tableBlock->m_TableValues.push_back(std::vector<TableCell>());
                ParseElement(node->children, hideSpoilers);
                break;
            }
            case NodeType::TableHeader:
            {
                std::shared_ptr<TableBlock> tableBlock = std::static_pointer_cast<TableBlock>(m_TextBlocks.back());
                std::vector<TableCell>& rowValues = tableBlock->m_TableValues.back();
                rowValues.push_back(TableCell());
                rowValues.back().m_IsHeader = true;
                rowValues.back().m_StringStream << "<b>";
                ParseElement(node->children, hideSpoilers);
                rowValues.back().m_StringStream << "</b>";
                break;
            }
            case NodeType::TableData:
            {
                std::shared_ptr<TableBlock> tableBlock = std::static_pointer_cast<TableBlock>(m_TextBlocks.back());
                std::vector<TableCell>& rowValues = tableBlock->m_TableValues.back();
                rowValues.push_back(TableCell());
                ParseElement(node->children, hideSpoilers);
                break;
            }
            case NodeType::Ul:
            {
                if (m_MaxTextLength != -1 && m_TextLength >= m_MaxTextLength)
                {
                    break;
                }

                switch (m_ParentNodeType)
                {
                    case NodeType::Table:
                    {
                        std::shared_ptr<TableBlock> tableBlock = std::static_pointer_cast<TableBlock>(m_TextBlocks.back());
                        std::vector<TableCell>& row = tableBlock->m_TableValues.back();
                        row.back().m_StringStream << '\n';
                        ParseElement(node->children, hideSpoilers);
                        row.back().m_StringStream << '\n';
                        break;
                    }
                    case NodeType::Quote:
                    case NodeType::Code:
                    case NodeType::None:
                    {
                        std::shared_ptr<TextBlock> textBlock = std::static_pointer_cast<TextBlock>(m_TextBlocks.back());
                        textBlock->m_StringStream << '\n';
                        ParseElement(node->children, hideSpoilers);
                        textBlock->m_StringStream << '\n';
                        break;
                    }
                    default:
                    {
                        ASSERT(false, "Unhandled m_ParentNodeType");
                    }
                }
                break;
            }
            case NodeType::Li:
            {
                if (m_MaxTextLength != -1 && m_TextLength >= m_MaxTextLength)
                {
                    break;
                }

                switch (m_ParentNodeType)
                {
                    case NodeType::Table:
                    {
                        std::shared_ptr<TableBlock> tableBlock = std::static_pointer_cast<TableBlock>(m_TextBlocks.back());
                        std::vector<TableCell>& row = tableBlock->m_TableValues.back();
                        row.back().m_StringStream << "• ";
                        ParseElement(node->children, hideSpoilers);
                        break;
                    }
                    case NodeType::Quote:
                    case NodeType::Code:
                    case NodeType::None:
                    {
                        std::shared_ptr<TextBlock> textBlock = std::static_pointer_cast<TextBlock>(m_TextBlocks.back());
                        textBlock->m_StringStream << "• ";
                        ParseElement(node->children, hideSpoilers);
                        break;
                    }
                    default:
                    {
                        ASSERT(false, "Unhandled m_ParentNodeType");
                        break;
                    }
                }
                break;
            }
            case NodeType::Hr:
            {
                if (m_MaxTextLength != -1 && m_TextLength >= m_MaxTextLength)
                {
                    break;
                }

                switch (m_ParentNodeType)
                {
                    case NodeType::Table:
                    {
                        std::shared_ptr<TableBlock> tableBlock = std::static_pointer_cast<TableBlock>(m_TextBlocks.back());
                        std::vector<TableCell>& row = tableBlock->m_TableValues.back();
                        row.back().m_StringStream << "--------\n\n";
                        ParseElement(node->children, hideSpoilers);
                        break;
                    }
                    case NodeType::Quote:
                    case NodeType::Code:
                    case NodeType::None:
                    {
                        std::shared_ptr<TextBlock> textBlock = std::static_pointer_cast<TextBlock>(m_TextBlocks.back());
                        textBlock->m_StringStream << "--------\n\n";
                        ParseElement(node->children, hideSpoilers);
                        break;
                    }
                    default:
                    {
                        ASSERT(false, "Unhandled m_ParentNodeType");
                        break;
                    }
                }
                break;
            }
            case NodeType::Br:
            {
                if (m_MaxTextLength != -1 && m_TextLength >= m_MaxTextLength)
                {
                    break;
                }

                switch (m_ParentNodeType)
                {
                    case NodeType::Table:
                    {
                        std::shared_ptr<TableBlock> tableBlock = std::static_pointer_cast<TableBlock>(m_TextBlocks.back());
                        std::vector<TableCell>& row = tableBlock->m_TableValues.back();
                        row.back().m_StringStream << "\n";
                        ParseElement(node->children, hideSpoilers);
                        break;
                    }
                    case NodeType::Quote:
                    case NodeType::Code:
                    case NodeType::None:
                    {
                        std::shared_ptr<TextBlock> textBlock = std::static_pointer_cast<TextBlock>(m_TextBlocks.back());
                        textBlock->m_StringStream << "\n";
                        ParseElement(node->children, hideSpoilers);
                        break;
                    }
                    default:
                    {
                        ASSERT(false, "Unhandled m_ParentNodeType");
                        break;
                    }
                }
                break;
            }
            case NodeType::Quote:
            {
                if (m_MaxTextLength != -1 && m_TextLength >= m_MaxTextLength)
                {
                    break;
                }

                m_ParentNodeType = NodeType::Quote;
                ParseElement(node->children, hideSpoilers);
                m_ParentNodeType = NodeType::None;
                break;
            }
            case NodeType::Code:
            {
                if (m_MaxTextLength != -1 && m_TextLength >= m_MaxTextLength)
                {
                    break;
                }

                m_ParentNodeType = NodeType::Code;
                ParseElement(node->children, hideSpoilers);
                m_ParentNodeType = NodeType::None;
                break;
            }
            case NodeType::Pre:
            {
                //ignore
                ParseElement(node->children, hideSpoilers);
                break;
            }
            case NodeType::Text:
            {
                if (html_text_to_pango_start.find(nodeName) != html_text_to_pango_start.end())
                {
                    switch (m_ParentNodeType)
                    {
                        case NodeType::Table:
                        {
                            std::shared_ptr<TableBlock> tableBlock = std::static_pointer_cast<TableBlock>(m_TextBlocks.back());
                            std::vector<TableCell>& row = tableBlock->m_TableValues.back();

                            row.back().m_StringStream << "<" << html_text_to_pango_start[nodeName];

                            xmlAttr* cur_attr = NULL;
                            for (cur_attr = node->properties; cur_attr; cur_attr = cur_attr->next)
                            {
                                row.back().m_StringStream << " " << cur_attr->name << "=\"";
                                ParseElement(cur_attr->children, hideSpoilers);
                                row.back().m_StringStream << "\"";
                            }

                            row.back().m_StringStream << ">";

                            ParseElement(node->children, hideSpoilers);

                            row.back().m_StringStream << "</" << html_text_to_pango_end[nodeName] << ">";
                            break;
                        }
                        case NodeType::Code:
                        case NodeType::Quote:
                        case NodeType::None:
                        {
                            std::shared_ptr<TextBlock> textBlock = std::static_pointer_cast<TextBlock>(m_TextBlocks.back());

                            textBlock->m_StringStream << "<" << html_text_to_pango_start[nodeName];

                            xmlAttr* cur_attr = NULL;
                            for (cur_attr = node->properties; cur_attr; cur_attr = cur_attr->next)
                            {
                                textBlock->m_StringStream << " " << cur_attr->name << "=\"";
                                ParseElement(cur_attr->children, hideSpoilers);
                                textBlock->m_StringStream << "\"";
                            }

                            textBlock->m_StringStream << ">";

                            ParseElement(node->children, hideSpoilers);

                            textBlock->m_StringStream << "</" << html_text_to_pango_end[nodeName] << ">";
                            break;
                        }
                        default:
                        {
                            ASSERT(false, "HtmlParser::ParseNode - Unhandled block type: %i", (int)blockType);
                        }
                    }
                }
                else
                {
                    if (strcmp((const char*)node->name, "span") == 0)
                    {
                        bool spoiler = false;
                        xmlAttr* cur_attr = NULL;
                        for (cur_attr = node->properties; cur_attr; cur_attr = cur_attr->next)
                        {
                            if (strcmp((const char*)cur_attr->name, "class") == 0)
                            {
                                if (cur_attr->children && strcmp((const char*)cur_attr->children->content, "md-spoiler-text") == 0)
                                {
                                    spoiler = true;
                                }
                            }
                        }

                        if (m_ParentNodeType == NodeType::Table)
                        {
                            ParseTableText(node->children, spoiler, hideSpoilers);
                        }
                        else
                        {
                            ParseText(node->children, spoiler, hideSpoilers);
                        }
                    }
                    else
                    {
                        ParseElement(node->children, hideSpoilers);
                    }
                }

                break;
            }
            default:
            {
                ASSERT(false, "HtmlParser::ParseNode - Unhandled node type: %i", (int)nodeType);
                break;
            }
        }

        if (beganSpecialBlock)
        {
            std::shared_ptr<TextBlock> textBlock = std::make_shared<TextBlock>();
            textBlock->m_Type = BlockType::Text;
            m_TextBlocks.push_back(textBlock);
        }
    }

    void HtmlParser::ParseText(xmlNode* node, bool spoiler, bool hideSpoilers)
    {
        if (m_MaxTextLength != -1 && m_TextLength >= m_MaxTextLength)
        {
            return;
        }

        std::shared_ptr<TextBlock> textBlock = std::static_pointer_cast<TextBlock>(m_TextBlocks.back());
        std::string content((char*) node->content, strlen((char*) node->content));
        m_TextLength += (int)content.length();
        EscapeText(content);
        if (spoiler && hideSpoilers)
        {
            if (AppSettings::Get()->GetBool("use_dark_theme", false))
            {
                textBlock->m_StringStream << "<span foreground=\"#000000\" background=\"#000000\">";
            }
            else
            {
                textBlock->m_StringStream << "<span foreground=\"#353535\" background=\"#000000\">";
            }
        }

        textBlock->m_StringStream << content;

        if (spoiler && hideSpoilers)
            textBlock->m_StringStream << "</span>";

        textBlock->m_ContainsSpoilers |= spoiler;

        ParseElement(node->children, hideSpoilers);
    }

    void HtmlParser::ParseTableText(xmlNode* node, bool spoiler, bool hideSpoilers)
    {
        if (m_MaxTextLength != -1 && m_TextLength >= m_MaxTextLength)
        {
            return;
        }

        std::shared_ptr<TableBlock> tableBlock = std::static_pointer_cast<TableBlock>(m_TextBlocks.back());
        std::vector<TableCell>& rowValues = tableBlock->m_TableValues.back();
        std::string content((char *)node->content, strlen((char *)node->content));
        m_TextLength += (int)content.length();
        EscapeText(content);
        if (spoiler && hideSpoilers)
            rowValues.back().m_StringStream << "<span foreground=\"black\" background=\"black\">";

        rowValues.back().m_StringStream << content;

        if (spoiler && hideSpoilers)
            rowValues.back().m_StringStream << "</span>";

        tableBlock->m_ContainsSpoilers |= spoiler;

        ParseElement(node->children, hideSpoilers);
    }
}