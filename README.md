# Headlines
## A GTK4/libAdwaita Reddit client written in C++

![screenshot](https://gitlab.com/caveman250/Headlines/raw/master/screenshots/headlines-1.png) ![screenshot](https://gitlab.com/caveman250/Headlines/raw/master/screenshots/headlines-2.png) ![screenshot](https://gitlab.com/caveman250/Headlines/raw/master/screenshots/headlines-3.png) 

# Install
## Flatpak
```
flatpak install flathub io.gitlab.caveman250.headlines
```

## Arch
```
git clone https://gitlab.com/caveman250/Headlines
cd Headlines/dist/aur
makepkg -si
```

# Building from source

## Build deps

```
- cmake
- gcc/g++
- gtkmm4 (unless you are using arch or alpine you will probably need to build this from source.)
- libmicrohttpd
- libcurl
- libssl
- libcrypto
- jsoncpp
- libxml2
- ffmpeg
- gstreamer
- gst-libav
- gst-plugins-base
- gst-plugins-good
- gst-plugins-bad
- youtube-dl
- boost
- websocketpp
- libadwaita
- xdg-utils (used to work around issues with gtk's link handler on phosh distros.)
```

## Steps

```
git clone https://gitlab.com/caveman250/Headlines
cd Headlines
mkdir bin && cd bin
cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -DDIST_BUILD=ON ..
make -j4
```
