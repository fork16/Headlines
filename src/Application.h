#pragma once

#include <UI/Pages/SavedPage.h>
#include "util/HttpClient.h"
#include "UI/CommentsView.h"
#include "UI/Widgets/Sidebar.h"
#include "API/posts_fwd.h"
#include "API/comments_fwd.h"
#include "UI/ui_types_fwd.h"

    namespace ui
    {
        class MainFeedContentProvider;
        class HeaderBar;
        class SearchPage;
        class SubredditPage;
        class UserPage;
        class NotificationBanner;
    }

    class Application
    {
    public:
        static int Run(int argc, char** argv);
        static Application* Get();
        static void Destroy();

        Application();

        void Init();
        void OnAppActivate();
        void Shutdown();

        const util::HttpClient& GetHttpClient() const { return m_HttpClient; }
        const Glib::RefPtr<Gtk::Application>& GetGtkApplication() const { return m_GtkApp; }
        Gtk::Window* GetWindow() const { return m_Window; }

        AdwLeaflet* GetLeaflet() const { return m_Stack; }
        AdwFlap* GetFlap() const { return m_Flap; }
        const ui::SidebarRef& GetSidebar() const { return m_Sidebar; }

        void AddPage(ui::PageRef page);
        void NavigateBackwards();
        void OnPageChanged();
        void ShowPage(const ui::Page* page);
        void PushPageStack(const ui::PageRef& page);
        ui::PageRef GetActivePage();
        std::vector<ui::PageRef> GetAllPagesOfType(ui::PageType type);
        ui::PopupRef GetPopup(bool ignoreLast) const;

        api::PostsType GetPostFeedSorting() const { return m_PostFeedSorting; }
        api::PostsType GetSubredditPostsSorting() const { return m_SubredditPostsSorting; }
        api::PostsType GetUserPostsSorting() const { return m_UserPostsSorting; }
        api::PostsType GetMultiPostsSorting() const { return m_MultiPostsSorting; }
        void SetCurrentPostsSortType(api::PostsType type);

        api::CommentsType GetCommentsViewSorting() const { return m_CommentsViewSorting; }
        api::CommentsType GetUserCommentsSorting() const { return m_UserCommentsSorting; }
        void SetCurrentCommentsSortType(api::CommentsType type);

        void ShowNotification(const std::string& message);

        void run_on_ui_thread(std::function<void()> fn);

    private:
        void SetupWindow(const Glib::RefPtr<Gtk::Builder>& builder);

        static Application* s_Instance;

        ui::SidebarRef m_Sidebar;
        AdwToastOverlay* m_ToastOverlay;

        Glib::RefPtr<Gtk::Application> m_GtkApp;
        util::HttpClient m_HttpClient;

        AdwLeaflet* m_Stack;
        AdwFlap* m_Flap;
        Gtk::Window* m_Window;

        std::vector<ui::PageRef> m_PageStack;

        api::PostsType m_PostFeedSorting;
        api::PostsType m_UserPostsSorting;
        api::PostsType m_SubredditPostsSorting;
        api::PostsType m_MultiPostsSorting;
        api::CommentsType m_CommentsViewSorting;
        api::CommentsType m_UserCommentsSorting;

        Glib::Dispatcher m_dispatcher;
        std::vector<std::function<void()>> m_dispatch_queue;
        std::mutex m_dispatch_mutex;
    };