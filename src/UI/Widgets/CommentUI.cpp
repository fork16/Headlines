#include <util/HtmlParser.h>
#include <Application.h>
#include <API/RedditAPI.h>
#include <util/SimpleThread.h>

#include <utility>
#include "CommentUI.h"
#include "API/Comment.h"
#include "UI/Popups/WriteCommentPopup.h"
#include "util/Helpers.h"
#include "HeaderBar.h"
#include "API/User.h"
#include "AppSettings.h"
#include "util/Colour.h"
#include "UI/Pages/SubredditPage.h"
#include "UI/Pages/UserPage.h"

namespace ui
{
    static util::Colour s_CommentColours[5] =
    {
        { 0.2, 0.4, 1 },
        { 0.2, 1, 0.5 },
        { 0.9, 0.9, 0.4 },
        { 1, 0.4, 0.4 },
        { 0.9, 0.4, 0.9 },
    };

    CommentUI::CommentUI(api::CommentRef comment, CommentsViewType commentsViewType, int depth)
        : m_Box(nullptr)
        , m_CommentTextBox(nullptr)
        , m_UsernameLabel(nullptr)
        , m_ChildrenBox(nullptr)
        , m_CollapseButton(nullptr)
        , m_MoreCommentsBox(nullptr)
        , m_MoreCommentsUIDispatcher()
        , m_ScoreLabel(nullptr)
        , m_UpVoteImage(nullptr)
        , m_DownVoteImage(nullptr)
        , m_BookmarkImage(nullptr)
        , m_Spinner(nullptr)
        , m_Comment(std::move(comment))
        , m_CommentsViewType(commentsViewType)
        , m_Depth(depth)
        , m_ChildrenVisible(true)
        , m_HasAddedUI(false)
        , m_RoundedCornersSettingChangedHandler(util::s_InvalidSignalHandlerID)
    {
        m_MoreCommentsUIDispatcher.connect([this]()
                                            {
                                                for (const CommentWidgetRef& childComment : m_Children)
                                                {
                                                    if (!childComment->m_Comment->IsPlaceholder() && !childComment->HasAddedUI())
                                                    {
                                                        childComment->CreateUI(m_ChildrenBox);
                                                    }
                                                }
                                            });
    }

    CommentUI::~CommentUI()
    {
        if (m_RoundedCornersSettingChangedHandler != util::s_InvalidSignalHandlerID)
        {
            util::Signals::Get()->RoundedCornersSettingChanged.RemoveHandler(m_RoundedCornersSettingChangedHandler);
        }
    }

    void CommentUI::CreateUI(Gtk::Box* parent, int index)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/comment.ui");

        m_Box = builder->get_widget<Gtk::Box>("CommentBox");
        if (m_CommentsViewType == CommentsViewType::UserComments || m_CommentsViewType == CommentsViewType::UserSavedComments)
        {
            util::Helpers::ApplyCardStyle(m_Box, AppSettings::Get()->GetBool("rounded_corners", false));
            m_RoundedCornersSettingChangedHandler = util::Signals::Get()->RoundedCornersSettingChanged.AddHandler([this](bool roundedCorners)
            {
                util::Helpers::ApplyCardStyle(m_Box, roundedCorners);
            });
            m_Box->set_margin_start(5);
            m_Box->set_margin_end(5);
        }
        else
        {
            m_Box->set_margin_start(m_Depth > 1 ? 5 : 0);
        }

        SetupCommentText(builder);
        SetupUsername(builder);
        SetupDate(builder);
        SetupVoteButtons(builder);
        SetupBookmarkButton(builder);
        SetupDeleteButton(builder);
        SetupReplyButton(builder);
        SetupDrawingArea(builder);
        SetupCollapseButton(builder);
        SetupShareButton(builder);
        SetupChildren(builder);
        SetupSecondRow(builder);

        m_HasAddedUI = true;

        if (index == -1)
        {
            parent->append(*m_Box);
        }
        else if (index == 0)
        {
            parent->prepend(*m_Box);
        }
        else if (index == 1)
        {
            parent->insert_child_after(*m_Box, *parent->get_first_child());
        }
    }

    void CommentUI::SetupCommentText(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        m_CommentTextBox = builder->get_widget<Gtk::Box>("CommentText");
        util::HtmlParser htmlParser;
        htmlParser.ParseHtml(m_Comment->GetText(), m_CommentTextBox, -1, true);
    }

    void CommentUI::SetupUsername(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        m_UsernameLabel = builder->get_widget<Gtk::Label>("UserNameText");
        Glib::RefPtr<Gtk::GestureClick> gestureClick = Gtk::GestureClick::create();
        m_UsernameLabel->add_controller(gestureClick);
        gestureClick->signal_released().connect([this](int n_press, double, double)
        {
            if (n_press == 0)
            {
                return;
            }

            if (m_CommentsViewType == CommentsViewType::UserComments)
            {
                ui::SubredditPageRef subView = std::make_shared<ui::SubredditPage>(m_Comment->GetSubreddit());
                Application::Get()->AddPage(subView);
            }
            else
            {
                ui::UserPageRef userView = std::make_shared<ui::UserPage>(m_Comment->GetUserName());
                Application::Get()->AddPage(userView);
            }
        });

        if (m_CommentsViewType == CommentsViewType::UserComments)
        {
            std::ostringstream oss;
            oss << "r/" << m_Comment->GetSubreddit();
            m_UsernameLabel->set_text(oss.str());
            m_UsernameLabel->get_style_context()->add_class("subreddit_text");
        }
        else
        {
            std::ostringstream oss;
            oss << "u/" << m_Comment->GetUserName();
            m_UsernameLabel->set_text(oss.str());
            if (m_Comment->IsModerator())
            {
                m_UsernameLabel->get_style_context()->add_class("moderator_text");
            }
            else if (m_Comment->IsOriginalPoster())
            {
                m_UsernameLabel->get_style_context()->add_class("op_text");
            }
            else
            {
                m_UsernameLabel->get_style_context()->add_class("username_text");
            }
        }
    }

    void CommentUI::SetupDate(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        m_DateLabel = builder->get_widget<Gtk::Label>("DateText");
        m_DateLabel->set_text(util::Helpers::TimeStampToTimeAndDateString(m_Comment->GetTimeStamp()));
    }

    void CommentUI::SetupVoteButtons(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        m_UpVoteImage = builder->get_widget<Gtk::Image>("UpvoteImage");
        m_DownVoteImage = builder->get_widget<Gtk::Image>("DownvoteImage");

        m_ScoreLabel = builder->get_widget<Gtk::Label>("ScoreText");
        UpdateScoreLabel();
        if (m_Comment->IsUpVoted())
        {
            m_UpVoteImage->get_style_context()->add_class("upvote");
        }
        else if (m_Comment->IsDownVoted())
        {
            m_DownVoteImage->get_style_context()->add_class("downvote");
        }

        Gtk::Button* upVoteButton;
        upVoteButton = builder->get_widget<Gtk::Button>("UpvoteBtn");
        upVoteButton->signal_clicked().connect([this]()
        {
            if (m_Comment->IsUpVoted())
            {
                RemoveUpVote();
            }
            else
            {
                UpVote();
            }
        });
        upVoteButton->set_sensitive(api::RedditAPI::Get()->IsLoggedIn());

        Gtk::Button* downVoteButton;
        downVoteButton = builder->get_widget<Gtk::Button>("DownvoteBtn");

        downVoteButton->signal_clicked().connect([this]()
        {
            if (m_Comment->IsDownVoted())
            {
                RemoveDownVote();
            }
            else
            {
                DownVote();
            }
        });
        downVoteButton->set_sensitive(api::RedditAPI::Get()->IsLoggedIn());
    }

    void CommentUI::SetupBookmarkButton(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        m_BookmarkImage = builder->get_widget<Gtk::Image>("BookmarkImage");
        if (m_Comment->IsBookmarked())
        {
            m_BookmarkImage->get_style_context()->add_class("remove_bookmark");
        }

        Gtk::Button* bookmarkButton;
        bookmarkButton = builder->get_widget<Gtk::Button>("BookmarkButton");
        bookmarkButton->set_visible(api::RedditAPI::Get()->IsLoggedIn());
        bookmarkButton->signal_clicked().connect([this]()
        {
            if (m_Comment->IsBookmarked())
            {
                m_Comment->RemoveBookmark();
                Application::Get()->ShowNotification("Comment un-saved.");
                m_BookmarkImage->get_style_context()->remove_class("remove_bookmark");
            }
            else
            {
                m_Comment->Bookmark();
                Application::Get()->ShowNotification("Comment saved.");
                m_BookmarkImage->get_style_context()->add_class("remove_bookmark");
            }
        });
    }

    void CommentUI::SetupDeleteButton(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        Gtk::Button* deleteButton = builder->get_widget<Gtk::Button>("DeleteButton");
        deleteButton->set_visible(api::RedditAPI::Get()->GetCurrentUser() && m_Comment->GetUserName() == api::RedditAPI::Get()->GetCurrentUser()->GetName());
        deleteButton->signal_clicked().connect([this]()
        {
            Gtk::MessageDialog* confirmDialog = new Gtk::MessageDialog("Are you sure you want to delete this comment?", false, Gtk::MessageType::WARNING, Gtk::ButtonsType::OK_CANCEL);
            confirmDialog->signal_response().connect([this, confirmDialog](int response)
            {
                if (response == Gtk::ResponseType::OK)
                {
                    api::RedditAPI::Get()->Delete(m_Comment->GetFullID());
                    if (!m_Comment->GetChildren().empty())
                    {
                        util::HtmlParser htmlParser;
                        std::vector<Gtk::Widget*> currTextNodes;
                        for (Gtk::Widget* child = m_CommentTextBox->get_first_child(); child != nullptr; child = child->get_next_sibling())
                        {
                            currTextNodes.push_back(child);
                        }
                        for (Gtk::Widget* widget : currTextNodes)
                        {
                            m_CommentTextBox->remove(*widget);
                        }

                        m_UsernameLabel->set_text("u/[deleted]");
                        htmlParser.ParseHtml("[deleted]", m_CommentTextBox, -1, true);
                    }
                    else
                    {
                        m_Box->set_visible(false);
                    }

                    Application::Get()->ShowNotification("Comment deleted.");
                }

                delete confirmDialog;
            });

            confirmDialog->set_transient_for(*Application::Get()->GetGtkApplication()->get_active_window());
            confirmDialog->show();
        });
    }

    void CommentUI::SetupReplyButton(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        Gtk::Button* replyButton;
        replyButton = builder->get_widget<Gtk::Button>("ReplyBtn");
        replyButton->set_visible(api::RedditAPI::Get()->IsLoggedIn());
        replyButton->signal_clicked().connect([this]()
        {
            auto sendHandler = [this](const std::string& commentText)
                    {
                Json::Value comment = api::RedditAPI::Get()->Comment(m_Comment->GetFullID(), commentText);
                Application::Get()->NavigateBackwards();

                api::CommentRef newComment = std::make_shared<api::Comment>(comment, m_Comment->GetKind());
                m_Comment->AddChild(newComment);
                CommentWidgetRef newCommentUI = std::make_shared<CommentUI>(newComment, m_CommentsViewType, m_Depth + 1);
                m_Children.push_back(newCommentUI);
                newCommentUI->CreateUI(m_ChildrenBox, 0);
                    };

            ui::WriteCommentPageRef replyView = std::make_shared<ui::WriteCommentPopup>(sendHandler);
            Application::Get()->AddPage(replyView);

            util::HtmlParser htmlParser;
            htmlParser.ParseHtml(m_Comment->GetText(), replyView->GetParentCommentBox(), -1, true);
        });
    }

    void CommentUI::SetupShareButton(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        m_ShareButton = builder->get_widget<Gtk::Button>("ShareBtn");
        m_ShareButton->signal_clicked().connect([this]()
        {
            Application::Get()->ShowNotification("Link copied to clipboard.");
            std::stringstream ss;
            ss << "https://reddit.com" << m_Comment->GetPermaLink();
            util::Helpers::CopyToClipboard(ss.str());
        });
    }

    void CommentUI::SetupCollapseButton(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        m_CollapseButton = builder->get_widget<Gtk::Button>("CollapseBtn");
        m_CollapseButton->signal_clicked().connect([this]()
        {
            this->ToggleChildren();
        });
        m_UnCollapseButton = builder->get_widget<Gtk::Button>("UnCollapseBtn");
        m_UnCollapseButton->signal_clicked().connect([this]()
        {
            this->ToggleChildren();
        });
    }

    void CommentUI::ToggleChildren()
    {
        if(m_ChildrenVisible) {
            m_ChildrenBox->hide();
            if (AppSettings::Get()->GetBool("full_comment_collapse", false))
            {
                m_DateLabel->hide();
                m_CommentTextBox->hide();
                m_ScoreLabel->hide();
                m_CommentSecondRow->hide();
                m_UnCollapseButton->show();
            }
        } else {
            m_ChildrenBox->show();
            m_DateLabel->show();
            m_CommentTextBox->show();
            m_ScoreLabel->show();
            m_CommentSecondRow->show();
            m_UnCollapseButton->hide();
        }
        m_ChildrenVisible = !m_ChildrenVisible;
        Gtk::Image* image = static_cast<Gtk::Image*>(m_CollapseButton->get_child());
        image->set_from_icon_name(m_ChildrenVisible ? "go-up-symbolic" : "go-down-symbolic");
    }

    void CommentUI::SetupDrawingArea(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        Gtk::DrawingArea* drawingArea;
        drawingArea = builder->get_widget<Gtk::DrawingArea>("CommentDrawingArea");
        if (m_Depth == 0)
        {
            drawingArea->set_visible(false);
        }
        else
        {
            drawingArea->set_draw_func([this](const Cairo::RefPtr<Cairo::Context>& cairoContext, int, int)
            {
                util::Colour colour = s_CommentColours[(m_Depth - 1) % 5];
                cairoContext->set_source_rgb(colour.GetR(), colour.GetG(), colour.GetB());
                cairo_paint((cairo_t *)cairoContext->cobj());
            });
        }
    }

    void CommentUI::SetupChildren(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        m_ChildrenBox = builder->get_widget<Gtk::Box>("ChildCommentBox");
        int childDepth = m_Depth + 1;
        for (const api::CommentRef& childComment : m_Comment->GetChildren())
        {
            if (!childComment->IsPlaceholder())
            {
                CommentWidgetRef commentUI = std::make_shared<CommentUI>(childComment, m_CommentsViewType, childDepth);
                commentUI->CreateUI(m_ChildrenBox);
                m_Children.push_back(commentUI);
            }
            else
            {
                AddMoreCommentsItem();
            }
        }
    }

    void CommentUI::SetupSecondRow(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        m_CommentSecondRow = builder->get_widget<Gtk::Box>("CommentSecondRow");
    }

    void CommentUI::AddMoreCommentsItem()
    {
        auto child_builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/more_comments.ui");
        m_MoreCommentsBox = child_builder->get_widget<Gtk::Box>("MoreComments");
        m_MoreCommentsBox->set_margin_start(m_Depth > 0 ? 5 : 0);
        Glib::RefPtr<Gtk::GestureClick> moreCommentsGestureClick = Gtk::GestureClick::create();
        m_MoreCommentsBox->add_controller(moreCommentsGestureClick);
        moreCommentsGestureClick->signal_released().connect([this](int n_press, double, double)
        {
            if (n_press == 0)
            {
                return;
            }
            LoadMoreCommentsAsync();
        });
        Gtk::DrawingArea* childDrawingArea;
        childDrawingArea = child_builder->get_widget<Gtk::DrawingArea>("DrawingArea");
        childDrawingArea->set_draw_func([this](const Cairo::RefPtr<Cairo::Context>& cairoContext, int, int)
        {
            util::Colour colour = s_CommentColours[m_Depth % 5];
            cairoContext->set_source_rgb(colour.GetR(), colour.GetG(), colour.GetB());
            cairo_paint((cairo_t *)cairoContext->cobj());
        });


        m_ChildrenBox->append(*m_MoreCommentsBox);
    }

    void CommentUI::LoadMoreCommentsAsync()
    {
        GThread* thread = g_thread_new("More Comments Download Thread", CommentUI::LoadMoreComments, (void*)this);
        g_thread_unref(thread);

        m_Spinner = new Gtk::Spinner();
        Gtk::Box* parentWidget = (Gtk::Box*)m_MoreCommentsBox->get_parent();

        parentWidget->remove(*m_MoreCommentsBox);
        m_MoreCommentsBox = nullptr;

        parentWidget->append(*m_Spinner);
        m_Spinner->show();
        m_Spinner->set_vexpand(true);
        m_Spinner->set_size_request(-1, 100);
        m_Spinner->start();
    }

    void* CommentUI::LoadMoreComments(void* args)
    {
        ((CommentUI*)args)->LoadMoreComments();
        return nullptr;
    }

    void CommentUI::LoadMoreComments()
    {
        std::vector<api::CommentRef> newChildren = api::RedditAPI::Get()->GetMoreComments(m_Comment->GetLinkID(), m_Depth + 3, m_Comment->GetFullID(), { m_Comment->GetID() });

        for (const api::CommentRef& child : newChildren)
        {
            const std::vector<api::CommentRef>& childComments = m_Comment->GetChildren();
            auto it = std::find_if(childComments.begin(), childComments.end(), [child](const api::CommentRef& a){ return a->GetFullID() == child->GetFullID(); });
            if (it == childComments.end())
            {
                m_Comment->AddChild(child);
                m_Children.push_back(std::make_shared<CommentUI>(child, m_CommentsViewType, m_Depth + 1));
            }
        }

        Gtk::Box* parentWidget = (Gtk::Box*)m_Spinner->get_parent();
        parentWidget->remove(*m_Spinner);
        delete m_Spinner;

        m_MoreCommentsUIDispatcher.emit();
    }

    void CommentUI::UpdateScoreLabel()
    {
        if (m_Comment->IsUpVoted())
        {
            m_ScoreLabel->get_style_context()->add_class("upvote");
            m_ScoreLabel->get_style_context()->remove_class("downvote");
        }
        else if (m_Comment->IsDownVoted())
        {
            m_ScoreLabel->get_style_context()->add_class("downvote");
            m_ScoreLabel->get_style_context()->remove_class("upvote");
        }
        else
        {
            m_ScoreLabel->get_style_context()->remove_class("upvote");
            m_ScoreLabel->get_style_context()->remove_class("downvote");
        }

        std::stringstream ss;
        ss << m_Comment->GetScore();
        m_ScoreLabel->set_label(ss.str());
    }

    void CommentUI::UpVote()
    {
        if (m_Comment->IsDownVoted())
        {
            m_DownVoteImage->get_style_context()->remove_class("downvote");
        }
        m_UpVoteImage->get_style_context()->add_class("upvote");
        m_Comment->UpVote();
        UpdateScoreLabel();
    }

    void CommentUI::RemoveUpVote()
    {
        m_UpVoteImage->get_style_context()->remove_class("upvote");
        m_Comment->RemoveUpVote();
        UpdateScoreLabel();
    }

    void CommentUI::DownVote()
    {
        if (m_Comment->IsUpVoted())
        {
            m_UpVoteImage->get_style_context()->remove_class("upvote");
        }

        m_DownVoteImage->get_style_context()->add_class("downvote");

        m_Comment->DownVote();
        UpdateScoreLabel();
    }

    void CommentUI::RemoveDownVote()
    {
        m_DownVoteImage->get_style_context()->remove_class("downvote");
        m_Comment->RemoveDownVote();
        UpdateScoreLabel();
    }

    void CommentUI::OnParentCommentDestroyed()
    {
        m_Box = nullptr;
        for (const CommentWidgetRef& commentUI : m_Children)
        {
            commentUI->OnParentCommentDestroyed();
        }
    }
}
