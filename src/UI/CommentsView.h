#pragma once

#include "UI/Pages/Page.h"
#include "API/Comment.h"
#include "UI/ui_types_fwd.h"

namespace util
{
    class SimpleThread;
    class ThreadWorker;
}

namespace api
{
    class Post;
}

namespace ui
{
    class PostWidget;

    enum class CommentsViewType
    {
        PostComments,
        UserComments,
        UserSavedComments,
        PostSpecificComment
    };

    class CommentsView : public std::enable_shared_from_this<CommentsView>
    {
    public:
        explicit CommentsView(CommentsViewType type);
        ~CommentsView();
        void CreateUI(Gtk::Viewport* viewport, Gtk::Box* parent);
        void Cleanup();
        UISettings GetUISettings();

        void LoadCommentsAsync(const std::string& url);
        void LoadComments(const std::string& url, util::ThreadWorker* worker);

        void LoadMoreCommentsAsync();
        void LoadMoreComments(util::ThreadWorker* worker);

        void ClearComments(bool clearPost = true);
        void Reload();

        Gtk::Box* GetCommentBox() { return m_CommentsBox; }
        void InsertUserComment(const api::CommentRef& comment);

        void SetPost(const api::PostRef& post, const std::string& contextComment);
        void SetUser(const api::UserRef& user);

    private:
        void AddFailedToLoadUI();

        CommentsViewType m_Type;

        void AddCommentsToUI();
        void OnViewportScrolled();

        std::vector<::api::CommentRef> m_Comments;
        std::vector<::api::CommentRef> m_PendingComments;
        std::vector<CommentWidgetRef> m_UIElements;

        Gtk::Box* m_CommentsBox = nullptr;
        std::mutex m_PendingCommentsMutex;
        Gtk::Spinner* m_Spinner = nullptr;
        Gtk::Viewport* m_Viewport = nullptr;
        Gtk::Overlay* m_FailedToLoadWidget = nullptr;
        Glib::Dispatcher m_FailedToLoadDispatcher;

        Glib::Dispatcher m_PendingCommentsDispatcher;

        api::PostRef m_Post;
        std::string m_ContextComment;
        ui::PostWidgetRef m_PostUI;

        api::UserRef m_User;

        //stagger adding comments to the UI so that the main thread doesnt lock.
        bool m_IsLoadingComments = false;
        std::string m_Url;

        util::SimpleThread* m_CommentsDownloadThread;
        std::vector<std::string> m_CommentsToDownLoad;
        std::string m_LastTimestamp; //used when looking at a users comments.
    };
}
