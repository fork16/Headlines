#include "WriteCommentPopup.h"
#include "UI/Widgets/HeaderBar.h"

namespace ui
{
    WriteCommentPopup::WriteCommentPopup(std::function<void(const std::string&)> sendButtonHandler)
        : Popup(PageType::WriteCommentPage, false)
        , m_SendButtonHandler(std::move(sendButtonHandler))
    {

    }

    void WriteCommentPopup::Cleanup()
    {
    }

    Gtk::Box* WriteCommentPopup::CreateUIInternal(AdwLeaflet* parent)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/write_comment.ui");

        Gtk::Box* box = builder->get_widget<Gtk::Box>("WriteCommentBox");

        m_ParentCommentBox = builder->get_widget<Gtk::Box>("ParentComment");
        m_TextView = builder->get_widget<Gtk::TextView>("MyCommentTextArea");
        m_Viewport = builder->get_widget<Gtk::Viewport>("Viewport");
        m_TextView->get_buffer()->signal_changed().connect([this]()
                                                           {
                                                               SetHeaderButtonEnabled(GTK_RESPONSE_ACCEPT, m_TextView->get_buffer()->size() > 0);
                                                           });

        Setup();

        AddPopupAction(PopupAction{ "Cancel", GTK_RESPONSE_CANCEL, true, [this]()
        { Close(); }});
        AddPopupAction(PopupAction{ "Reply", GTK_RESPONSE_ACCEPT, false, [this]()
        { m_SendButtonHandler(GetText()); }});

        CreatePopup(parent, box, "Reply");

        return box;
    }

    void WriteCommentPopup::Setup()
    {
        m_TextView->get_buffer()->set_text("");
        m_TextView->grab_focus();
        m_Viewport->get_vadjustment()->set_value(m_Viewport->get_vadjustment()->get_upper());
    }

    UISettings WriteCommentPopup::GetUISettings() const
    {
        return UISettings();
    }

    void WriteCommentPopup::OnPopupChoiceSelected(GtkResponseType response)
    {
        switch (response)
        {
            case GTK_RESPONSE_ACCEPT:
                m_SendButtonHandler(GetText());
                // fall through
            case GTK_RESPONSE_CANCEL:
                Close();
                break;
            default:
                g_warning("SubmitPostPopup::OnPopupChoiceSelected - Unhandled response type: %d", response);
                break;
        }
    }
}