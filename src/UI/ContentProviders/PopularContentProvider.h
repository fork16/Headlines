#pragma once
#include "RedditContentProvider.h"

namespace ui
{
    class PostWidget;
    class PopularContentProvider : public RedditContentProvider
    {
    public:
        PopularContentProvider();
        virtual ~PopularContentProvider() {}

    private:
        virtual std::vector<RedditContentListItemRef> GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker) override;
        virtual std::string GetFailedToLoadErrorMsg() override;
    };
}
