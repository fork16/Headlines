#pragma once

#include "API/Post.h"
#include "Page.h"

namespace util
{
    class CancellationToken;
}

namespace ui
{
    class ImagePage : public Page
    {
    public:
        explicit ImagePage(const std::vector<util::ImageCollection>& imagePath, const std::string& title);
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;
        virtual void Reload() override { /*ignore*/ }
        virtual void Cleanup() override;
        virtual SortType GetSortType() const override { return SortType::None; }
        virtual UISettings GetUISettings() const override;
        virtual void OnSaveButton() override;

    private:

        virtual void OnHeaderBarCreated() override;
        std::string m_Title;
        const std::vector<util::ImageCollection>& m_Images;
        std::vector<std::string> m_ImageFilePaths;
        Gtk::Notebook* m_Notebook = nullptr;
        Gtk::Spinner* m_Spinner = nullptr;

        int m_OriginalPictureWidth;
        int m_OriginalPictureHeight;
        float m_Scale = 1.f;

        util::CancellationToken* m_cancellation_token = nullptr;
        bool m_ImageDownloaded = false;
    };
}
