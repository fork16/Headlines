#include "AwardWidget.h"
#include "PictureWidget.h"

namespace ui
{
    AwardWidget::AwardWidget(const api::Award& award)
    {
        m_Child = new Gtk::Box(Gtk::Orientation::HORIZONTAL, 5);
        m_Child->set_parent(*this);
        m_Child->set_tooltip_text(util::Helpers::FormatString("%s: %s", award.GetName().c_str(), award.GetDescription().c_str()));

        ui::PictureWidget* pictureWidget = new ui::PictureWidget(32, 32, false, false);
        util::ImageData data = util::ImageData(award.GetImageUrl());
        pictureWidget->SetImageData(data);
        m_Child->append(*pictureWidget);

        Gtk::Label* label = new Gtk::Label();
        label->set_text(util::Helpers::FormatString("x%d", award.GetCount()));
        m_Child->append(*label);
    }

    void AwardWidget::size_allocate_vfunc(int width, int height, int baseline)
    {
        Gdk::Rectangle rect = Gdk::Rectangle();
        rect.set_x(0);
        rect.set_y(0);
        rect.set_width(width);
        rect.set_height(height);

        if (m_Child)
        {
            m_Child->size_allocate(rect, baseline);
        }
    }

    void AwardWidget::measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const
    {
        if (m_Child)
        {
            m_Child->measure(orientation, for_size, minimum, natural, minimum_baseline, natural_baseline);
        }
    }

    void AwardWidget::snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot)
    {
        if (m_Child)
        {
            snapshot_child(*m_Child, snapshot);
        }
    }

    Gtk::SizeRequestMode AwardWidget::get_request_mode_vfunc() const
    {
        return Gtk::SizeRequestMode::HEIGHT_FOR_WIDTH;
    }
}