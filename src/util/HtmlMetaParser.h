#include <regex>
#include <libxml/HTMLparser.h>

namespace util
{
    class HtmlMetaParser
    {
    public:
    public:
        static std::map<std::string, std::string> get_metadata(const std::string& url);

    private:
        static void download_html(const std::string& url, const std::string& out_file_name);
    };
}
