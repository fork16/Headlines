#pragma once

#include "RedditContentProvider.h"

namespace ui
{
    enum MessagesType
    {
        Inbox,
        Sent
    };
    class MessagesContentProvider : public RedditContentProvider
    {
    public:
        MessagesContentProvider(MessagesType type);
    private:
        virtual std::vector<RedditContentListItemRef> GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker) override;
        virtual std::string GetFailedToLoadErrorMsg() override;

        MessagesType m_Type;
    };
}
