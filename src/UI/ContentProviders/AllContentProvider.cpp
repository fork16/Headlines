#include <AppSettings.h>
#include "AllContentProvider.h"
#include "UI/Widgets/PostWidget.h"
#include "API/RedditAPI.h"
#include "Application.h"
#include "util/ThreadWorker.h"

namespace ui
{
    AllContentProvider::AllContentProvider()
    {
    }

    std::vector<RedditContentListItemRef> AllContentProvider::GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker)
    {
        std::vector<::api::PostRef> posts = ::api::RedditAPI::Get()->GetAllPosts(Application::Get()->GetPostFeedSorting(), AppSettings::Get()->GetInt("num_posts_to_load", 5), lastTimeStamp);
        std::vector<RedditContentListItemRef> uiElements;
        for (const api::PostRef& post : posts)
        {
            if (worker->IsCancelled())
            {
                return uiElements;
            }

            PostWidgetRef postUI = std::make_shared<PostWidget>(post);
            uiElements.push_back(postUI);
        }

        return uiElements;
    }

    std::string AllContentProvider::GetFailedToLoadErrorMsg()
    {
        return "No posts found. Retry?";
    }
}